import numpy as np


class KNN:
    """
    An online version of the k-Nearest Neighbors algorithm.
    "Online" here means it handles new data points one at a time
    and incrementally updates the model each time.

    For a description of the k-NN algorithm, check out
    https://youtu.be/KluQCQtHTqk
    """
    def __init__(
        self,
        k=5,
        is_classifier=True,
        max_node_points=1e3,
    ):
        """
        Arguments

        k, int
            The star of the show. How many neighbors will be used
            to make each estimate.

        is_classifier, boolean
            True: This instance of k-NN is going to be used for classification.
            False: it's going to be used for regression (estimating a
                floating point value).

        max_node_points, int
            The maximum number of data points that will be allowed in
            a single node. This will be the largest driver of how fast the
            algorithm runs. A smaller number will run faster but might be
            a bit less accurate.
        """
        self.is_classifier = is_classifier
        self.k = int(k)
        self.n_features = None
        self.max_node_points = int(max_node_points)
        self.n_points = int(0)
        self.points = None

        # The number of points used to estimate the variance.
        # This can be different than the number of points retained
        # in the model.
        self.n_iter = 1
        self.feature_means = None
        self.sum_square_diffs = None
        self.stddevs = None

    def initialize(self):
        self.n_features = int(self.target.size)
        self.point_tree = PointTreeNode(
            max_node_points=self.max_node_points,
            n_features=self.n_features)

        self.sum_square_diffs = np.ones(self.n_features)
        self.stddevs = np.ones(self.n_features)
        self.feature_means = self.target.copy()
        self.previous_feature_means = self.target.copy()


    def update(self, target=None, target_label=None):
        """
        The algorithm handles one example at a time.
        Whatever shape the forward input has, it will be assumed to
        be a set of feature values corresponding to the current
        example and will be flattened.
        """

        self.target = target
        self.target_label = target_label

        if self.n_features is None:
            self.initialize()

        self.n_iter += 1
        self.update_variance_estimates(self.target)

        # Estimate the correct class for the new point
        labels, distances = self.point_tree.find_k_nearest(self.target, self.k)

        if labels is None:
            estimate = None
            confidence = 0
        else:
            estimate, confidence = self.estimate_target(labels, distances)

        if self.target_label is not None:
            # Check that an identical point doesn't already exist.
            add_point = True
            epsilon = 1e-6
            if distances is not None:
                for i in range(len(distances)):
                    if (np.abs(distances[i]) < epsilon and
                            labels[i] == self.target_label):
                        add_point = False
            if add_point:
                self.point_tree.add_point(self.target, self.target_label)
                self.n_points += 1

        return estimate

    def update_variance_estimates(self, target):
        """
        Calculate a running estimate of the variance.

        I lifted the method entirely from here:
        https://fanf2.user.srcf.net/hermes/doc/antiforgery/stats.pdf
        """
        self.previous_feature_means = self.feature_means
        self.feature_means = ((
            target + self.previous_feature_means *
            (self.n_iter - 1)) /
            self.n_iter)
        self.sum_square_diffs = (
            self.sum_square_diffs +
            (target - self.feature_means) *
            (target - self.previous_feature_means))
        self.stddevs = np.sqrt(self.sum_square_diffs / self.n_iter)

    def estimate_target(self, labels, distances):
        """
        Based on the nearest neighbors and how close they are,
        estimate the value of the target.

        Weight each neighbor's vote based on distance.
        The closer it is, the more say it gets.

        If it's a floating point value (regression), the estimate
        comes from a weighted average of the neighbors' labels.

        If it's a category (classification), the estimate comes
        from a weighted election among the neighbors' labels.
        """
        # Handle the case where there aren't yet enough data points.
        if labels is None or distances is None:
            return None, 0

        if labels[0] is None or distances[0] is None:
            return None, 0

        if len(distances) < self.k:
            return None, 0

        # Taking the log of the distance gives us a scale-independent
        # version of the distance. It will help kNN work well across
        # a wide range of sampling densities.
        distances = np.array(distances)
        log_distances = np.log(distances + 1)
        vote_weights = 1 / (log_distances + 1)

        if self.is_classifier:
            # kNN is expected to estimate a category.
            # Create a ballot box for each category.
            # Collect the votes from each neighbor.
            predictions = {}
            for i in range(len(labels)):
                label = labels[i]
                if label in predictions.keys():
                    predictions[label] += vote_weights[i]
                else:
                    predictions[label] = vote_weights[i]

            estimated_label = None
            highest_vote = 0
            second_vote = 0
            for label, votes in predictions.items():
                if votes > highest_vote:
                    second_vote = highest_vote
                    highest_vote = votes
                    estimated_label = label
            confidence = highest_vote - second_vote

        else:
            # kNN is expected to estimate a real value.
            total_votes = 0
            total_weights = 0
            for i_index, i_label in enumerate(indices):
                total_votes += self.labels[i_label] * vote_weights[i_index]
                total_weights += vote_weights[i_index]
            estimated_label = total_votes / total_weights
            confidence = 1

        return estimated_label, confidence


class PointTreeNode:
    def __init__(
        self,
        n_features=None,
        is_classifier=True,
        max_node_points=int(1e3),
    ):
        self.is_leaf = True
        self.is_classifier = is_classifier
        self.i_split = None
        self.split_value = None
        self.n_points = 0
        self.n_features = n_features
        self.max_node_points = int(max_node_points)
        self.points = np.zeros((self.max_node_points, self.n_features))
        self.labels = [None] * self.max_node_points
        self.hi_branch = None
        self.lo_branch = None

    def find_k_nearest(self, target, k):
        """
        Find the distance between the test point
        and each of the training points.
        Use the Manhattan distance, the sum of differences in each dimension,
        a.k.a. the L1 norm or Mahalanobis distance if you like sounding fancy.

        The L1 norm is better suited to high dimensional data (having more
        than a couple of columns) than the L2 or Euclidean norm.
        In enough dimensions, the L2 norm tends to put almost everything
        the sae distance away from each other. (It's counterintuitive and
        weird. Ask a mathematician.) The L1 norm keeps things a bit more
        friendly to finding meaningful neighbors.
        """
        if self.is_leaf:
            if self.n_points <= k:
                return None, None

            distance = np.sum(np.abs(
                self.points[:self.n_points, :] - target[np.newaxis, :]),
                axis=1)
            i_partitioned = np.argpartition(distance, k - 1)
            i_top_k = i_partitioned[:k]

            distance_top_k = []
            label_top_k = []
            for i in i_top_k:
                distance_top_k.append(distance[i])
                label_top_k.append(self.labels[i])
        else:
            if target[self.i_split] > self.split_value:
                branch = self.hi_branch
            else:
                branch = self.lo_branch

            label_top_k, distance_top_k = branch.find_k_nearest(target, k)

        return label_top_k, distance_top_k


    def add_point(self, target, label):
        # Add the new point to the collection of previously observed points
        if self.is_leaf:
            self.points[int(self.n_points), :] = target
            self.labels[int(self.n_points)] = label
            self.n_points += 1

            if self.n_points >= self.max_node_points - 1:
                self.split()
        else:
            if target[self.i_split] > self.split_value:
                branch = self.hi_branch
            else:
                branch = self.lo_branch
            branch.add_point(target, label)

    def split(self):
        self.is_leaf = False
        # Test each feature for split quality.
        i_best_feature = -1
        q_best_feature = 1e10
        best_split_value = None
        for i_feature in range(self.n_features):
            q_feature, split_value = self.find_split_quality(i_feature)
            if q_feature < q_best_feature:
                q_best_feature = q_feature
                i_best_feature = i_feature
                best_split_value = split_value

        self.i_split = i_best_feature
        self.split_value = best_split_value
        self.lo_branch = PointTreeNode(
            is_classifier=self.is_classifier,
            n_features=self.n_features,
            max_node_points=self.max_node_points)
        self.hi_branch = PointTreeNode(
            is_classifier=self.is_classifier,
            n_features=self.n_features,
            max_node_points=self.max_node_points)

        # Divy up the points and labels
        for i in range(self.n_points):
            if self.points[i, self.i_split] > self.split_value:
                self.hi_branch.add_point(self.points[i, :], self.labels[i])
            else:
                self.lo_branch.add_point(self.points[i, :], self.labels[i])
        self.n_points = 0
        self.points = None
        self.labels = None

    def find_split_quality(self, i_feature):
        feature_points = self.points[:, i_feature]
        split_value = np.median(feature_points)
        i_lo = np.where(feature_points <= split_value)
        i_hi = np.where(feature_points > split_value)
        min_group_size = np.minimum(
            np.log(i_lo[0].size + 1),
            np.log(i_hi[0].size + 1))
        split_quality = (
            self.group_quality(feature_points[i_lo]) +
            self.group_quality(feature_points[i_hi])) * min_group_size
        return split_quality, split_value

    def group_quality(self, values):
        if self.is_classifier:
            # Count the incidence of each category
            categories = {}
            for value in values:
                if value in categories.keys():
                    categories[value] += 1
                else:
                    categories[value] = 1
            # Entropy
            freqs = np.array(list(categories.values())) / values.size
            quality = np.sum(freqs * np.log(freqs))
        else:
            # Variance
            quality = -np.var(values)
        return quality
