import numpy as np
from oknn.knn import KNN


class MultiviewKNN(KNN):
    """
    An extension the k-Nearest Neighbors algorithm that makes use of
    a single example having multiple views, for example,
    multiple photos a single object or rotated versions of a single image.

    To see a full list of the initialization arguments, look at the
    KNN class in knn.py.
    """
    def update(self, views=None, target_label=None):
        """
        The algorithm handles one example at a time,
        but the example will be represented by multiple views.
        The views will be contained in a list.

        Whatever shape each view of the forward input has,
        it will be assumed to
        be a set of feature values corresponding to the current
        example and will be flattened.
        """
        # Make sure all the views are flattened.
        self.views = [view.ravel() for view in views]
        self.target_label = target_label

        # Pull out the first view.
        # It will be called self.target to maintain consistency with
        # the original KNN block.
        self.target = np.array(self.views[0]).ravel()

        if self.n_features is None:
            self.initialize()

        self.n_iter += 1
        for view in self.views:
            self.update_variance_estimates(view)

        # Estimate the correct class for the new point.
        # Consider all the views equally.
        # Gather up the k-nearest neighbors *for each view*.
        labels_list = []
        distances_list = []
        votes = {}

        for view in self.views:
            labels, distances = self.point_tree.find_k_nearest(view, self.k)
            if labels is None:
                estimate = None
                confidence = 0
            else:
                estimate, confidence = self.estimate_target(labels, distances)
            distances_list.append(distances)
            labels_list.append(labels)

            if confidence > 0:
                if estimate in votes.keys():
                    votes[estimate] += confidence
                else:
                    votes[estimate] = confidence
        # Estimate the correct class for the new point
        highest_vote = 0
        estimate = None
        for label, vote in votes.items():
            if vote > highest_vote:
                highest_vote = vote
                estimate = label

        if self.target_label is not None:
            for i_view in range(len(self.views)):
                # Check that an identical point doesn't already exist.
                add_point = True
                identical_point_added = False
                view_distances = distances_list[i_view]
                view_labels = labels_list[i_view]
                epsilon = 1e-6
                if view_distances is not None:
                    for i in range(len(view_distances)):
                        if (np.abs(view_distances[i]) < epsilon
                                and identical_point_added):
                            add_point = False
                        if (np.abs(view_distances[i]) < epsilon and
                                view_labels[i] == self.target_label):
                            add_point = False
                        else:
                            identical_point_added = True
                if add_point:
                    self.point_tree.add_point(
                        self.views[i_view],
                        self.target_label)
                    self.n_points += 1

        return estimate
