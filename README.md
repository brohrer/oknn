# OkNN

An online implementation of k-Nearest Neighbors, with some tweaks.

Install at the command line with 

```bash
git clone git@gitlab.com:brohrer/oknn.git
python3 -m pip install -e oknn
```
