import os
import setuptools as st

with open("README.md", "r") as fh:
    long_description = fh.read()

st.setup(
    name='oknn',
    version='2021.06.04',
    description='online k-nearest neighbors',
    url='http://github.com/brohrer/oknn',
    download_url='https://github.com/brohrer/oknn/tags/',
    author='Brandon Rohrer',
    author_email='brohrer@gmail.com',
    license='MIT',
    install_requires=[
        'matplotlib',
        'mnist',
        'numba',
        'numpy',
    ],
    long_description=long_description,
    long_description_content_type="text/markdown",
    package_data={
        "": [
            "README.md",
            "LICENSE",
            # os.path.join("data", "penguins.csv")
        ],
    },
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
